# Jersey Service

Example of a service using Jersey and gradle.

It also includes an example on how to use the builder pattern.

## TODO

* Add swagger docs.
* Implement endpoint security.
* Dockerize and add k8s configuration to create service.
* Configure gitlab CI.