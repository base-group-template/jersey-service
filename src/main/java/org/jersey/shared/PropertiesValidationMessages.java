package org.jersey.shared;

public final class PropertiesValidationMessages {
    public static final String NULL_OR_EMPTY_PROPERTY = "The property '%s' can't be null or empty";
}
