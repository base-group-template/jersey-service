package org.jersey.config;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.jersey.helloworld.HelloWorldResource;
import org.jersey.helloworld.repository.HelloWorldRepository;
import org.jersey.helloworld.repository.HelloWorldRepositoryImpl;
import org.jersey.helloworld.service.HelloWorldService;
import org.jersey.helloworld.service.HelloWorldServiceImpl;
import org.jersey.other.OtherResource;

public final class ResourceConfiguration {
    public static ResourceConfig init() {
        ResourceConfig resourceConfig = new ResourceConfig(
                HelloWorldResource.class,
                OtherResource.class
        );

        resourceConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(HelloWorldServiceImpl.class).to(HelloWorldService.class);
                bind(HelloWorldRepositoryImpl.class).to(HelloWorldRepository.class);
            }
        });

        return resourceConfig;
    }
}
