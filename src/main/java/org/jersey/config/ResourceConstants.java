package org.jersey.config;

import java.net.URI;

public final class ResourceConstants {
    public static final URI BASE_URI = URI.create("http://localhost:8080");

    public static final String HELLO_WORLD_RESOURCE_PATH = "/hello-world";
    public static final String OTHER_RESOURCE_PATH = "/other";
}
