package org.jersey;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.jersey.config.ResourceConfiguration;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.jersey.config.ResourceConstants.BASE_URI;

public class Application extends ResourceConfig {


    public static HttpServer createServer() {
        return GrizzlyHttpServerFactory
                .createHttpServer(BASE_URI, ResourceConfiguration.init(), false);
    }

    public static void main(String[] args) {
        try {
            final HttpServer server = createServer();

            Runtime.getRuntime().addShutdownHook(new Thread(server::shutdownNow));

            server.start();

            Thread.currentThread().join();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}