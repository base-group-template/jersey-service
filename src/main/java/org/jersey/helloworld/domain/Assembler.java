package org.jersey.helloworld.domain;

import org.jersey.helloworld.entity.HelloWorld;
import org.jersey.helloworld.entity.HelloWorldImpl;

public class Assembler {
    public HelloWorld toEntity(HelloWorldDto helloWorldDto) {
        return new HelloWorldImpl(helloWorldDto.getMessage());
    }

    public HelloWorldDto toDomain(HelloWorld helloWorld) {
        return HelloWorldDtoImpl.builder()
                .setMessage(helloWorld.getMessage())
                .build();
    }
}
