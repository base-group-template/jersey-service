package org.jersey.helloworld.domain;

public interface HelloWorldDto {

    String getMessage();

    String toString();
}
