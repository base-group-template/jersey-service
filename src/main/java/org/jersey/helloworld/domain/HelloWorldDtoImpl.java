package org.jersey.helloworld.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.commons.lang3.StringUtils;

import static org.jersey.shared.PropertiesValidationMessages.NULL_OR_EMPTY_PROPERTY;

@JsonDeserialize(builder = HelloWorldDtoImpl.Builder.class)
public class HelloWorldDtoImpl implements HelloWorldDto {
    private final String message;

    private HelloWorldDtoImpl(Builder builder) {
        this.message = builder.message;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "{" +
                "   message: " + message +
                "}";
    }

    public static class Builder {

        private String message;

        @JsonProperty("message")
        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public HelloWorldDto build() {
            HelloWorldDto helloWorldDto = new HelloWorldDtoImpl(this);
            validateUserObject(helloWorldDto);
            return helloWorldDto;
        }

        private void validateUserObject(HelloWorldDto helloWorldDto) {

            if (StringUtils.isBlank(helloWorldDto.getMessage())) {
                throw new IllegalArgumentException(String.format(NULL_OR_EMPTY_PROPERTY, "message"));
            }
        }
    }
}
