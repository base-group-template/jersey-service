package org.jersey.helloworld.entity;

public class HelloWorldImpl implements HelloWorld {
    private String message;

    public HelloWorldImpl() {
    }

    public HelloWorldImpl(String message) {
        this.message = message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "{" +
                "   message: " + message +
                "}";
    }
}
