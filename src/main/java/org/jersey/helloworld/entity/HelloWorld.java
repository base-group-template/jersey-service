package org.jersey.helloworld.entity;

public interface HelloWorld {

    String getMessage();

    void setMessage(String message);

    String toString();
}
