package org.jersey.helloworld.service;

import jakarta.inject.Inject;
import org.jersey.helloworld.domain.Assembler;
import org.jersey.helloworld.domain.HelloWorldDto;
import org.jersey.helloworld.repository.HelloWorldRepository;
import org.jvnet.hk2.annotations.Service;

@Service
public class HelloWorldServiceImpl implements HelloWorldService {

    private static final Assembler da = new Assembler();

    @Inject
    private HelloWorldRepository helloWorldRepository;

    @Override
    public HelloWorldDto helloworld() {
        return da.toDomain(helloWorldRepository.getHelloWorld());
    }
}
