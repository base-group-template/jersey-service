package org.jersey.helloworld.service;

import org.jersey.helloworld.domain.HelloWorldDto;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface HelloWorldService {
    HelloWorldDto helloworld();
}
