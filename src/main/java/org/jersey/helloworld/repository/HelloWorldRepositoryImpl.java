package org.jersey.helloworld.repository;

import org.jersey.helloworld.entity.HelloWorld;
import org.jersey.helloworld.entity.HelloWorldImpl;
import org.jvnet.hk2.annotations.Service;

@Service
public class HelloWorldRepositoryImpl implements HelloWorldRepository {
    @Override
    public HelloWorld getHelloWorld() {
        return new HelloWorldImpl("Hello, World!");
    }
}
