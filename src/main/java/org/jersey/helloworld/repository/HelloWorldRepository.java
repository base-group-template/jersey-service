package org.jersey.helloworld.repository;

import org.jersey.helloworld.entity.HelloWorld;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface HelloWorldRepository {
    HelloWorld getHelloWorld();
}
