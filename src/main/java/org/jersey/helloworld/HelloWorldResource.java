package org.jersey.helloworld;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jersey.helloworld.service.HelloWorldService;

import static org.jersey.config.ResourceConstants.HELLO_WORLD_RESOURCE_PATH;

@Path(HELLO_WORLD_RESOURCE_PATH)
public class HelloWorldResource {

    @Inject
    private HelloWorldService helloWorldService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response helloWorld() {
        return Response.ok(helloWorldService.helloworld()).build();
    }
}