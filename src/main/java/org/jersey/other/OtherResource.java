package org.jersey.other;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import static org.jersey.config.ResourceConstants.OTHER_RESOURCE_PATH;

@Path(OTHER_RESOURCE_PATH)
public class OtherResource {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response other() {
        return Response.ok("other").build();
    }
}
