package org.jersey.helloworld;

import org.jersey.BaseResourceTestSetUp;
import org.jersey.helloworld.domain.HelloWorldDto;
import org.jersey.helloworld.domain.HelloWorldDtoImpl;
import org.junit.jupiter.api.Test;

import static org.jersey.config.ResourceConstants.HELLO_WORLD_RESOURCE_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelloWorldResourceShould extends BaseResourceTestSetUp {

    @Test
    void return_hello_world_message() {
        HelloWorldDto response = target.path(HELLO_WORLD_RESOURCE_PATH).request().get(HelloWorldDtoImpl.class);
        assertEquals("Hello, World!", response.getMessage());
    }

}
