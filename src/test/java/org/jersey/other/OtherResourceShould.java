package org.jersey.other;

import org.jersey.BaseResourceTestSetUp;
import org.junit.jupiter.api.Test;

import static org.jersey.config.ResourceConstants.OTHER_RESOURCE_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OtherResourceShould extends BaseResourceTestSetUp {

    @Test
    void return_other() {
        String response = target.path(OTHER_RESOURCE_PATH).request().get(String.class);
        assertEquals("other", response);
    }

}
