package org.jersey;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import static org.jersey.config.ResourceConstants.BASE_URI;

public class BaseResourceTestSetUp {
    public static WebTarget target;
    private static HttpServer httpServer;

    @BeforeAll
    public static void beforeAllTests() {
        httpServer = Application.createServer();
        Client client = ClientBuilder.newClient();
        target = client.target(BASE_URI);
    }

    @AfterAll
    public static void afterAllTests() {
        httpServer.shutdownNow();
    }
}
